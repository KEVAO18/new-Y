<?php
session_start();

function Head($lugar=''){
  if (!isset($_SESSION['user'])) {
    header("location: ../Home.php");
  }
  echo "
<!DOCTYPE html>
<html lang='en' class='full-height'>
  <head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel='icon' href='".$lugar."assets/media/favicon.png'>
    <title>YEY - ".$_SESSION['user']."</title>
    <!-- Font Awesome -->
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.2/css/all.css'>
    <!-- Bootstrap core CSS -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
    <!-- Material Design Bootstrap -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/css/mdb.min.css' rel='stylesheet'>
    <!-- css -->
    <link rel='stylesheet' href='".$lugar."assets/css/styles1.css'>
  </head>";
}

function Navbar($lugar=''){
  echo "<body>
    <nav class='navbar navbar-expand-lg navbar-dark black sticky-top scrolling-navbar'>
      <a class='navbar-brand' href='#'><strong>YEAH</strong></a>
      <div class='container'>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
          <span class='navbar-toggler-icon'></span>
        </button>
        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul class='navbar-nav mr-auto'>
            <li class='nav-item a-h'>
              <a class='nav-link' href='#'>Home<span class='sr-only'>(current)</span></a>
            </li>
            <li class='nav-item a-h'>
              <a class='nav-link' href='#'>profile</a>
            </li>
            <li class='nav-item a-h'>
              <a class='nav-link' href='#'>settings</a>
            </li>
            <li class='nav-item a-h'>
              <a class='nav-link' href='".$lugar."assets/funciones/partes/exit.php'>Exit</a>
            </li>
          </ul>
          <form class='form-inline my-2 my-lg-0'>
            <input class='form-control mr-sm-2' type='search' placeholder='Search' aria-label='Search'>
            <button id='send1' class='btn btn-sm bg-white text-black'>Search</button>
          </form>
          <ul class='navbar-nav nav-flex-icons'>
            <li class='nav-item'>
              <a class='nav-link'><i class='fab fa-facebook-f'></i></a>
            </li>
            <li class='nav-item'>
              <a class='nav-link'><i class='fab fa-twitter'></i></a>
            </li>
            <li class='nav-item'>
              <a class='nav-link'><i class='fab fa-instagram'></i></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>";
}

function footer($lugar=''){
  echo "<!-- Footer -->
      <footer class='page-footer font-small blue-grey lighten-5'>

        <!-- Copyright -->
        <div class='footer-copyright text-center text-black-50 py-3'>© 2020 Copyright:
          <a class='dark-grey-text' href='https://kevaosportafolio.000webhostapp.com'>Kevin O.</a>
        </div>
        <!-- Copyright -->

      </footer>
    <!-- Footer -->
    <!-- ajax -->
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <!-- JQuery -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <!-- Bootstrap tooltips -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js'></script>
    <!-- Bootstrap core JavaScript -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js'></script>
    <!-- MDB core JavaScript -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/js/mdb.min.js'></script>
    <!-- scripts -->
    <script type='text/javascript' src='".$lugar."assets/javascript/script1.js'></script>
    ";
}

function finish($lugar=''){
  echo "
  </body>
</html>
";}