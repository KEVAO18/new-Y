//Register modal

$(document).on("ready", Register());

function Register(){
  $("#send1").on("click", function(e){
    e.preventDefault();
    var frm = $("#formReg").serialize();
    $.ajax({
      type: "POST",
      url: "assets/funciones/partes/conec.php?op=1",
      data: frm
    }).done(function (info) {
      if (info == "Complete") {
        document.getElementById('p-reg').innerHTML = "Good, now SignIn";
        document.getElementById('alt-reg').classList.add('alert-success');
        alert("Welcome");
      }else{
        document.getElementById('p-reg').innerHTML = info;
        document.getElementById('alt-reg').classList.add('alert-danger');
      }
    })
  });
}

//signin modal

$(document).on("ready", SignIn());

function SignIn(){
  $("#send2").on("click", function(e){
    e.preventDefault();
    var frm = $("#formSig").serialize();
    $.ajax({
      type: "POST",
      url: "assets/funciones/partes/conec.php?op=2",
      data: frm
    }).done(function (info) {
      if (info == "logging in") {
        window.location.href = 'logged/';
      }else{
        document.getElementById('p-sig').innerHTML = info;
        document.getElementById('alt-sig').classList.add('alert-danger');
      }
    })
  });
}

//helper modal

$(document).on("ready", Help());

function Help() {
  $("#send3").on("click", function (e) {
    e.preventDefault();
    var frm = $("#formHelp").serialize();
    $.ajax({
      type: "POST",
      url: "assets/funciones/partes/help.php?envio=true",
      dataType: "json",
      data: frm
    }).done(function (info) {
      console.log( info )
    })
  });
}

//search