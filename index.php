<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title id="title">Loaded</title>
	<link rel='icon' href='assets/media/favicon.png'>
    <!-- Font Awesome -->
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.2/css/all.css'>
    <!-- Bootstrap core CSS -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
    <!-- Material Design Bootstrap -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/css/mdb.min.css' rel='stylesheet'>
	<style>
body{
    margin: 0;
    padding: 0;
    background-color: #262626;
}

@keyframes animate{
    0%{
        background-position: 0px 100px;
    }
    40%{
        background-position: 1000px -70px;
    }
    80%{
        background-position: 1500px -80px;
    }
    100%{
        background-position: 1000px 100px;
    }
}

.cup{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 150px;
    height: 180px;
    border: 6px solid #262626;
    border-top: 2px solid transparent;
    border-radius: 15px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-image: url(https://i.ibb.co/gSXvJ35/water.png);
    background-repeat: repeat-x;
    animation: animate 6s linear infinite;
    box-shadow: 0 0 0 6px #FFF, 0 20px 35px rgba(0,0,0,1);
}

.cup::before{
    content: '';
    position: absolute;
    width: 50px;
    height: 80px;
    border: 6px solid #FFF;
    right: -56px;
    top: 40px;
    border-top-right-radius: 35px;
    border-bottom-right-radius: 35px;
}
	</style>
</head>
<body>
	<div class="cup"></div>
	<script>
	setTimeout(function() {
		document.getElementById('title').innerHTML = "Loaded.";
	}, 0);
	setTimeout(function() {
		document.getElementById('title').innerHTML = "Loaded..";
	}, 1000);
	setTimeout(function() {
		document.getElementById('title').innerHTML = "Loaded...";
	}, 2000);
	setTimeout(function() {
		document.getElementById('title').innerHTML = "Go";
	}, 3000);
	setTimeout(function() {
		document.getElementById('title').innerHTML = "Welcome";
		window.location.href = 'Home.php';
	}, 4000);
	</script>
</body>
</html>