<?php
include '../assets/funciones/partes/cuerpo.php';
include '../assets/funciones/partes/post.php';

	Head("../");
	navbar("../");
?>

  <div class='container-fluid'>
    <div class='row my-5 py-5'>
    	<div class="col-md-2">
    	</div>
      <div class='col-md-6'>
		<div class="row py-3">
			<div class="col-12">
				<h5>New Post</h5>
			</div>
			<div class="col-3 text-center">
				<a href="#" class="dark-grey-text" title="terms and conditions" id='modalActivate' type='button' data-toggle='modal' data-target='#Terms'><i class="fas fa-align-center"></i></a>
			</div>
			<div class="col-3">
				<a href="#" class="dark-grey-text" title="terms and conditions" id='modalActivate' type='button' data-toggle='modal' data-target='#Terms'><i class="fas fa-images"></i></a>
			</div> 
			<div class="col-3">3</div>
			<div class="col-3">4</div>
		</div>

		<!-- Post Text-->
	    <div class="modal fade right" id="Terms" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
	      <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	          <div class="modal-header black text-white">
	            <h5 class="modal-title" id="exampleModalPreviewLabel">New Post</h5>
	            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">&times;</span>
	            </button>
	          </div>
	          <div class="modal-body">
	            
	          </div>
	        </div>
	      </div>
	    </div>
	    <!-- Modal -->

      	<div class="card">
	  		<div class="card-header">
	  			<a href="" data-toggle="tooltip" data-placement="top" title="Profile" id="namePost"><img src="../assets/media/pp/<?php echo $_SESSION['user']; ?>.png" class="rounded-circle z-depth-1" style="width: 6%;" alt="profile photo"> Nombre del publicador</a>
	  		</div>
	  		<div class="card-body p-0">
	  			<img src="../assets/media/YEY2.png" style="width: 100%; height: 100vh; " alt="">
	  		</div>
	  		<div class="card-footer text-muted">

			    <a class="px-2 fa-lg" data-toggle="tooltip" data-placement="top" title="Like"><i id="like" class="far fa-thumbs-up"></i></a>
			    <!-- <i class="fas fa-thumbs-up"></i> para poder cambiar el like cambia far por fas-->

			    <a class="px-2 fa-lg" data-toggle="tooltip" data-placement="top" title="dislike"><i id="dislike" class="far fa-thumbs-down"></i></a>
			    <!-- <i class="fas fa-thumbs-down"></i> para poder cambiar el like cambia far por fas-->

			    <a class="px-2 fa-lg" data-toggle="tooltip" data-placement="top" title="comments"><i class="fas fa-comments"></i></a>

			    <a class="px-2 fa-lg" data-toggle="tooltip" data-placement="top" title="share"><i class="fas fa-share"></i></a>
        	</div>
      	</div>
      </div>
      <div class="col-md-1"></div>
      <div class='col-md-3 fijar'>
        <div class='card text-center'>
          <div class="flex-center py-3">
          	<img src="../assets/media/pp/<?php echo $_SESSION['user']; ?>.png" class="rounded-circle z-depth-1" style="width: 40%;" alt="profile photo">
          </div>
        	<div class="p-4">
        		<h4 id="name"></h4>
        		<h5 id="user" class="dark-grey-text">User: </h5>
        		<h5 id="age" class="dark-grey-text">Age: </h5>
        	</div>
        <div class="card-footer text-muted">
        	<p id="email" class="dark-grey-text"></p>
        </div>
        </div>
      </div>
    </div>
  </div>
<?php
	footer("../");
	$data = array('name' =>  $_SESSION['name'], 'user' =>  $_SESSION['user']);
?>
<script>
//alert("La resolución de tu pantalla es: " + $( window ).width());
//resoluccion optima es 1080 para el minwidth
	var user = <?php echo json_encode($data);?>;
	$(document).on("ready", $.ajax({
		type: "POST",
		url: "../assets/funciones/partes/optain.php?op=<?php echo $_SESSION['control'];?>",
		dataType: "json",
		data: user
	}).done(
		function (info) {
			document.getElementById('name').innerHTML += info.name;
			document.getElementById('user').innerHTML += "@<?php echo $data['user']; ?>";
			document.getElementById('email').innerHTML += info.email;
			document.getElementById('age').innerHTML += info.age;
		})
	);
	// Tooltips Initialization
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	</script>

<?php
	finish("../");
?>
