<?php

  session_start();
  if (isset($_SESSION['user'])) {
    header("location: logged/");
  }

?>
<!DOCTYPE html>
<html lang='en' class='full-height'>
  <head>
  	<meta charset='utf-8'>
  	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel='icon' href='assets/media/favicon.png'>
  	<title>YEY</title>
    <!-- Font Awesome -->
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.2/css/all.css'>
    <!-- Bootstrap core CSS -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
    <!-- Material Design Bootstrap -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/css/mdb.min.css' rel='stylesheet'>
    <!-- css -->
    <link rel='stylesheet' href='assets/css/styles1.css'>
  </head>
  <body>
    <nav class='navbar navbar-expand-lg navbar-dark black sticky-top scrolling-navbar'>
      <a class='navbar-brand' href='#'><strong>YEAH</strong></a>
      <div class='container'>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
          <span class='navbar-toggler-icon'></span>
        </button>
        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul class='navbar-nav mr-auto'>
            <li class='nav-item a-h'>
              <a class='nav-link' href='#' id='modalActivate' type='button' data-toggle='modal' data-target='#SignIn'>Sign In<span class='sr-only'>(current)</span></a>
            </li>
            <li class='nav-item a-h'>
              <a class='nav-link' href='#' id='modalActivate' type='button' data-toggle='modal' data-target='#Register'>Register</a>
            </li>
            <li class='nav-item a-h'>
              <a class='nav-link' href='#' id='modalActivate' type='button' data-toggle='modal' data-target='#More'>Help</a>
            </li>
          </ul>
          <ul class='navbar-nav nav-flex-icons'>
            <li class='nav-item'>
              <a class='nav-link'><i class='fab fa-facebook-f'></i></a>
            </li>
            <li class='nav-item'>
              <a class='nav-link'><i class='fab fa-twitter'></i></a>
            </li>
            <li class='nav-item'>
              <a class='nav-link'><i class='fab fa-instagram'></i></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Modal SignIn-->
    <div class='modal fade right' id="SignIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
      <div class="modal-dialog modal-full-height modal-right" role="document">
        <div class="modal-content">
          <div class="modal-header black text-white">
            <h5 class="modal-title" id="exampleModalPreviewLabel">SignIn</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="container" id="formSig" role="form">
              <div class="alert" id="alt-sig" role="alert">
                <p id="p-sig"></p><a href="#" id="a-sig" class="alert-link"></a>
              </div>
              <div class='md-form mt-3'>
                <input type='text' id='userOrEmailSig' class='form-control' name='userOrEmailSig' required>
                <label for='userOrEmailSig'> <i class="fa fa-user grey-text"></i> - Username or email</label>
              </div>
              <div class='md-form mt-3'>
                <input type='password' id='passSig' class='form-control' name='passSig' required>
                <label for='passSig'> <i class="fas fa-key grey-text"></i> - Password</label>
              </div>
              <button id="send2" class="btn black text-white">Ok</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->

    <!-- Modal Register-->
    <div class="modal fade right" id="Register" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
      <div class="modal-dialog modal-full-height modal-right" role="document">
        <div class="modal-content">
          <div class="modal-header black text-white">
            <h5 class="modal-title" id="exampleModalPreviewLabel">Register</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="container" id="formReg" role="form">
              <div class="alert" id="alt-reg" role="alert">
                <p id="p-reg"></p><a href="#" id="a-reg" class="alert-link"></a>
              </div>
              <div class='md-form mt-3'>
                <input type='text' id='nameReg' class='form-control' name='nameReg' required>
                <label for='nameReg'> <i class="fa fa-user grey-text"></i> - Name</label>
              </div>
              <div class='md-form mt-3'>
                <input type='text' id='userReg' class='form-control' name='userReg' required>
                <label for='userReg'> <i class="fa fa-user grey-text"></i> - Username</label>
              </div>
              <div class='md-form mt-3'>
                <input type='text' id='emailReg' class='form-control' name='emailReg' required>
                <label for='emailReg'> <i class="fa fa-envelope grey-text"></i> - E-mail</label>
              </div>
              <div class='md-form mt-3'>
                <input type='password' id='passReg' class='form-control' name='passReg' required>
                <label for='passReg'> <i class="fas fa-key grey-text"></i> - Password</label>
              </div>
              <div class='md-form mt-3'>
                <input type='password' id='repassReg' class='form-control' name='repassReg' required>
                <label for='repassReg'> <i class="fas fa-key grey-text"></i> - Re Password</label>
              </div>
              <div class='md-form mt-3'>
                <input type='number' id='ageReg' class='form-control' name='ageReg' required>
                <label for='ageReg'> <i class="fas fa-sort-numeric-up grey-text"></i> - Age</label>
              </div>
              <div class='md-form mt-3 text-center'>
                <small>At the time of registration, you agree to all of our terms and conditions.</small>
              </div>
              <button id="send1" class="btn black text-white">Ok</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->

    <!-- Modal More-->
    <div class="modal fade right" id="More" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
      <div class="modal-dialog modal-side modal-top-right" role="document">
        <div class="modal-content">
          <div class="modal-header black text-white">
            <h5 class="modal-title" id="exampleModalPreviewLabel">Help</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="container" id="formHelp" role="form">
              <div class="md-form mt-3">
                <input type="email" class="form-control" name="nameHelp" id="nameHelp" required>
                <label for="nameHepl">Name</label>
              </div>
              <div class="md-form mt-3">
                <input type="email" class="form-control" name="emailHelp" id="emailHelp" required>
                <label for="emailHepl">E-mail</label>
              </div>
              <div class="md-form">
                <textarea id="textHelp" class="md-textarea form-control" rows="10" name="textHelp"></textarea>
                <label for="form10">how can we help you?</label>
              </div>
              <button id="send3" class="btn black text-white">Ok</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->

    <!-- Modal terms and conditions-->
    <div class="modal fade right" id="Terms" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header black text-white">
            <h5 class="modal-title" id="exampleModalPreviewLabel">Terms and Conditions</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <embed src="Contributor-Covenant-Code-of-Conduct.pdf" type="application/pdf" width="100%" height="600px" />
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->

    <!--Main Navigation-->
    <header>
      <div class='view intro-2'>
        <div class='full-bg-img'>
          <div class='mask rgba-black-strong flex-center'>
            <div class='container'>
              <div class='white-text text-center wow fadeInUp'>
                <h5>Welcome to</h5>
                <h1>Yeah English Yeah</h1>
                <h5>the social network that also teaches you english</h5>
                <a href='' class='btn black' title='Sign In' id="modalActivate" type="button" data-toggle="modal" data-target="#SignIn">Sign In</a>
                or
                <a href='' class='btn black' title='Sign In' id="modalActivate" type="button" data-toggle="modal" data-target="#Register">Register</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section>
      <article class="black">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 py-4 text-center">

              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-center text-light">
                <h1 class="py-4 "><strong>More Information</strong></h1>
              </div>
              <div class="col-md-4 col-sm-12 py-4 text-center">
                <div class="card p-4">
                  <h1 class=""><i class="fas fa-globe"></i></h1>
                  <h3 class=""><strong>We're</strong></h3>
                  <h4 class="">We are a platform to share with people from all over the world sharing languages ​​and even cultures, everything this combined with exams proposed by us and your friends (with a previous review)</h4>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 py-4 text-center">
                <div class="card p-4">
                  <h1 class=""><i class="fas fa-chart-bar"></i></h1>
                  <h3 class=""><strong>Registered User Accountant</strong></h3>
                  <h4 id="contador">It will be activated as soon as we reach 500 users.</h4>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 py-4 text-center">
                <div class="card p-4">
                  <h1 class=""><i class="fas fa-graduation-cap"></i></h1>
                  <h3 class=""><strong>We Want</strong></h3>
                  <h4 class="">We want to teach languages using this social network and connecting our users with people from other countries</h4>
                </div>
              </div>
            </div>
          </div>
      </article>
    </section>
    <!-- Footer -->
      <footer class='page-footer font-small blue-grey lighten-5'>

        <div style='background-color: #000000;'>
          <div class='container'>

            <!-- Grid row-->
            <div class='row py-4 d-flex align-items-center'>

              <!-- Grid column -->
              <div class='col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0'>
                <h6 class='mb-0'>Are you ready to learn? Come with us.</h6>
              </div>
              <!-- Grid column -->

              <!-- Grid column -->
              <div class='col-md-6 col-lg-7 text-center text-md-right'>

                <!-- Facebook -->
                <a class='fb-ic'>
                  <i class='fab fa-facebook-f white-text mr-4'> </i>
                </a>
                <!-- Twitter -->
                <a class='tw-ic'>
                  <i class='fab fa-twitter white-text mr-4'> </i>
                </a>
                <!--Instagram-->
                <a class='ins-ic'>
                  <i class='fab fa-instagram white-text'> </i>
                </a>
                <!-- GitHub -->
                <a class='git-ic'>
                  <i class='fab fa-github-alt'> </i>
                </a>

              </div>
              <!-- Grid column -->

            </div>
            <!-- Grid row-->

          </div>
        </div>

        <!-- Footer Links -->
        <div class='container text-center text-md-left mt-5'>

          <!-- Grid row -->
          <div class='row mt-3 dark-grey-text'>

            <!-- Grid column -->
            <div class='col-md-3 col-lg-4 col-xl-3 mb-4'>

              <!-- Content -->
              <h6 class='text-uppercase font-weight-bold'>Yeah English Yeah</h6>
              <hr class='black accent-3 mb-4 mt-0 d-inline-block mx-auto' style='width: 60px;'>
              <p>the social network that also teaches you Languages</p>
              <p><strong>For a better World</strong></p>
              <a href="#" class="dark-grey-text" title="terms and conditions" id='modalActivate' type='button' data-toggle='modal' data-target='#Terms'>Terms And Conditions</a>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class='col-md-2 col-lg-2 col-xl-2 mx-auto mb-4'>

              <!-- Links -->
              <h6 class='text-uppercase font-weight-bold'>Founder</h6>
              <hr class='black accent-3 mb-4 mt-0 d-inline-block mx-auto' style='width: 60px;'>
              <p>
                <a class='dark-grey-text' href='https://kevaosportafolio.000webhostapp.com' title='portfolio' target='_blank'>Kevin Andrés Orrego Martínez</a>
              </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class='col-md-3 col-lg-2 col-xl-2 mx-auto mb-4'>

              <!-- Links -->
              <h6 class='text-uppercase font-weight-bold'>Useful links</h6>
              <hr class='black accent-3 mb-4 mt-0 d-inline-block mx-auto' style='width: 60px;'>
              <p>
                <a class='dark-grey-text' href='#' id='modalActivate' type='button' data-toggle='modal' data-target='#SignIn'>Sign In</a>
              </p>
              <p>
                <a class='dark-grey-text' href='#' id='modalActivate' type='button' data-toggle='modal' data-target='#Register'>Register</a>
              </p>
              <p>
                <a class='dark-grey-text' href='#' id="modalActivate" type="button" data-toggle='modal' data-target='#More'>Help</a>
              </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class='col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4'>

              <!-- Links -->
              <h6 class='text-uppercase font-weight-bold'>Contact</h6>
              <hr class='black accent-3 mb-4 mt-0 d-inline-block mx-auto' style='width: 60px;'>
              <p>
                <i class='fas fa-home mr-3'></i>Medellín - Antioquia Cll72 N°42 - 14</p>
              <p>
                <i class='fas fa-envelope mr-3'></i> yeahenglishyeah@gmail.com</p>
              <p>
                <i class='fas fa-phone mr-3'></i> +57 312 689 9514</p>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Footer Links -->

        <!-- Copyright -->
        <div class='footer-copyright text-center text-black-50 py-3'>© 2020 Copyright:
          <a class='dark-grey-text' href='https://kevaosportafolio.000webhostapp.com'>Kevin O.</a>
        </div>
        <!-- Copyright -->

      </footer>
    <!-- Footer -->

    <!-- ajax -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- JQuery -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <!-- Bootstrap tooltips -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js'></script>
    <!-- Bootstrap core JavaScript -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js'></script>
    <!-- MDB core JavaScript -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/js/mdb.min.js'></script>
    <!-- scripts -->
    <script type="text/javascript" src="assets/javascript/script1.js"></script>
    <script>
      //contador
      var user = {'contar': true};
      $(document).on("ready", contador());

      function contador(){
        $.ajax({
          type: "POST",
          url: "assets/funciones/partes/conec.php?op=3",
          dataType: "json",
          data: user
        }).done(
          function (info) {
            //document.getElementById('contador').innerHTML = info.count;
          }
        );
      }
      setInterval(contador, 20000);
    </script>
  </body>
</html>